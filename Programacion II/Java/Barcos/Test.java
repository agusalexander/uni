public class Test {

	public static void main(String[] args) {
	
		Mapa m1 = new Mapa(5);
		m1.asignarNombre(0, "B01");
		m1.asignarNombre(1, "B02");
		m1.asignarNombre(2, "B03");
		m1.asignarNombre(3, "B04");
		m1.asignarNombre(4, "B05");
		m1.setDistEsp(0, 30 );  // cambia a 30 y mira lo que pasa con estrategia a  
		m1.setDistEsp(1, 21 ); 
		m1.setDistEsp(2, 13 ); 
		m1.setDistEsp(3, 12 ); 
		m1.setDistEsp(4, 12 );
		m1.setDistBarcos(0, 1, 5, true); //i = indice del barco actual j= indice del barco al cual se averigua la distancia
		m1.setDistBarcos(0, 2, 6, true); //i = id actual j = id al cual acceder 
		m1.setDistBarcos(0, 3, 7, true); 
		m1.setDistBarcos(0, 4, 9, true);
		m1.setDistBarcos(1, 2, 10, true); 
		m1.setDistBarcos(1, 3, 15, true); 
		m1.setDistBarcos(1, 4, 30, true); 
		m1.setDistBarcos(2, 3, 15, true); 
		m1.setDistBarcos(2, 4, 25, true); 
		m1.setDistBarcos(3, 4, 5, true); 

		//System.out.println(m1.distancia(m1.caminoMinimoProbable()));
		//System.out.println(m1.caminoMinimoProbable());
		//m1.caminoMinimoProbableDosPuntoCero();
		m1.caminoMinimoProbable();
	}

}
