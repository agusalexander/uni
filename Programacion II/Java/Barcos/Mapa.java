
import java.util.ArrayList;

public class Mapa {
private Barco [] barcos;
private int [][] distancias;  // distancia de i a j
private int [] distanciaEspejo; // distancia al espejo

private int n;

public Mapa(int tamano){
	n= tamano;
	barcos = new Barco [n];	
	distancias = new int [n][n];
	distanciaEspejo = new int [n];
	
	for (int i=0; i<n; i++){
		barcos [i] = new Barco(i,"Barco" + i);
		distanciaEspejo[i] = 0;
		for (int j=0; j<n; j++){
			distancias[i][j] = 0;	//distancias default en 0
	
		}
	}
	
}


public int tamano(){
	return n;
}


public void asignarNombre(Integer NumerodeBarco, String nombre){
	barcos[NumerodeBarco].Nombre = nombre;
}

public void setDistBarcos(int i, int j, int dist, boolean simetrico){ 
	distancias[i][j] = dist;
	
	if (simetrico){
		distancias[j][i] = dist;
	}
	
}

public void setDistEsp(int i, int dist){ 
	distanciaEspejo[i] = dist;

}

// distancias nominales
public int getDistBarcos(int i, int j){ // si es adyacente tomamos ady[i][j]
return distancias[i][j];
}

private  int distanciaMaxima(){
	int Maximo=distancias[0][1];
	for (int i=0; i<n; i++){
		for (int j=0; j<n; j++){
			
			if ((distancias[i][j]>Maximo)&&(distancias[i][j]!=0)){
				Maximo=distancias[i][j];
			};	//distancias default en 0
	
		}
	}
	return Maximo;
	
}



public ArrayList<Barco> caminoMinimoProbable(){
	ArrayList <Barco> camino = new ArrayList<Barco>();
	Barco actual = masCercanoEspejo();
	//System.out.println("El barco mas cercano al espejo es:"+actual);
	camino.add(actual);
	actual.quemado=true;
	int Maximo=distanciaMaxima();
	
	//Barco siguiente =
	
		for (int x=0;x<this.distancias.length-1;x++){
		
			actual=buscarSiguiente(camino,actual.id,Maximo);
		    
			camino.add(actual);
		}	
			
	System.out.print(camino);		
	return camino;
}



private  Barco buscarSiguiente(ArrayList<Barco> camino,int index,int Maximo){
	int indiceMenor=0;
	int DistanciaMenor=Maximo; //this.distancias[index][0];
	
	for (int i = 0  ; i < this.distancias[index].length;i++){
		
		if(!camino.contains(this.barcos[i])){
			
			if (this.distancias[index][i]<=DistanciaMenor){
				DistanciaMenor=this.distancias[index][i];
				indiceMenor=i;
				
				
			}
		}
	}
	//if (this.barcos[indiceMenor].id==00){System.out.println(this.barcos[indiceMenor].quemado);}
	if(indiceMenor<0){
	throw new RuntimeException ("Error no hay botes o hay "+n+" botes en el mismo lugar");}
	Barco nuevo=barcos[indiceMenor];
	return nuevo;
}
//primer barco que se agrega al camino
private Barco masCercanoEspejo(){
	Barco cercano = null;
	int indMenor = 0;
	int min = this.distanciaEspejo[0];
	if(this.distanciaEspejo.length == 0){
		throw new RuntimeException ("Error 404. Boats not found");
	}
	//ya que se asume que el primer barco es el mas cercano al espejo
	//reviso los siguientes, se alterna el uso del arreglo de distancia barcos 
	//y arreglo de barcos 
	for (int x = 1; x < this.distanciaEspejo.length; x++){
		if (this.distanciaEspejo[x] < min){
			min = this.distanciaEspejo[x];
			indMenor = x;
		}
		cercano = this.barcos[indMenor];
		
	}
	return cercano;		
}


private Barco barcoMasCercano(Integer barcoOrigen){
	//...
	return null;
}




ArrayList<Barco> caminoMinimoProbableDosPuntoCero(){ //cambiar nombre agus, esta piola pero no da este nombre (?)
	ArrayList<Barco> camino= new ArrayList<Barco> ();
	//el mas cercano se agrega
	Barco actual = masCercanoEspejo();
	camino.add(actual);
	
	//busco la distancia mayor al espejo
	int lejano= this.lejano();
	
	//recorre las distancias siguientes (no recorro todo el arreglo de barcos, ya que se
	//puede repetir el primer barco , ya que este fue agregado previamente)
	for(int i = 0; i < this.barcos.length-1; i++){
		Barco siguiente = this.sigCercanoEspejo(camino, lejano);
		camino.add(siguiente);
	}
	
	//System.out.print(camino);
	return camino;
}
//decuelve la distancia mas lejana al espejo
private int lejano(){
	int lejano= this.distanciaEspejo[0];
	for(int i = 1; i < this.distanciaEspejo.length; i++ ){
		if( this.distanciaEspejo[i] > lejano){
			lejano=this.distanciaEspejo[i];
		}
	}
	return lejano;
}
//busca el barco mas cercano al espejo para eso toma la distancia mas lejana al espejo
private Barco sigCercanoEspejo(ArrayList<Barco> camino,int maximo){
	int cercano = maximo, indMenor = 0;
	
	boolean esta=false; //indica si el barco a analizar esta ya en el camino
	
	for( int i = 0; i < distanciaEspejo.length; i++){
		esta=camino.contains(this.barcos[i]); //metodo de arrayList
		if(!esta){
			if(this.distanciaEspejo[i] <= cercano){ //si no esta verifica que la distancia sea menor o igual
				cercano = distanciaEspejo[i];		//ya que puede haber mas de una igual
				indMenor = i;						
				
			}
		}
	}
	Barco nuevo = this.barcos[indMenor];  //toma el barco correspondiente a su ubicacion en el arreglo de barcos
	return nuevo;
}





Integer  distancia(ArrayList<Barco> camino){

	Integer _distancia = 0;

	if (camino.size() > 0){
		_distancia = distanciaEspejo[camino.get(0).id];
	}
	
			
	for (int i=0;i< camino.size()-1;i++){
		_distancia = _distancia + distancias[camino.get(i).id][camino.get(i+1).id];
	}

	return _distancia;
	
}



}
