

//import.awt.Component;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.MenuBar;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import entorno.*;
@SuppressWarnings("unused")
public class Juego extends InterfaceJuego
{
	//RESOLUCION
	public static Entorno entorno;
	public static Point RES=new Point(1024,768);
	//VARS auxiliares
	private static int xp1,yp1,xp2=RES.x/2,yp2; // importante guarda posicion del mouse si lo sacas de pantalla
	private static int x=0;
	private static int y=0;
	private static int tiempoDeCarga=300;
	static int contTicks;

	//CURSOR
	
	Image Cursor1=Herramientas.cargarImagen("FlechaP1.png");
	//Image Espada=Herramientas.cargarImagen("Espada.png");
	Image Fondo=Herramientas.cargarImagen("Fondo.jpg");
	private Toolkit tk=Toolkit.getDefaultToolkit();

	
	//Cursor Ataque = tk.createCustomCursor(Espada, new Point(x,y), "trans");
	Cursor Normal = tk.createCustomCursor(Cursor1, new Point(x,y), "trans");
	


	 //Jugadores , el nombre del jugador es importante no cambiarlo
	static Jugador Jugador1=new Jugador("Jugador1");
	static Jugador Gaia=new Jugador();
	

	static Object Adler=new Object(RES.x/2,RES.y/2,RES.y/5);
	static Object Pantalla=new Object(0,0,RES.x,RES.y);
	
	public static Object[] Objetos={Adler};
	
	Color blanco=new Color(255,255,255);
	private boolean Up=false;
	private boolean Down=false;
	private boolean Left=false;
	private boolean Right=false;
	//private Image Fondo=Herramientas.cargarImagen("Fondo.jpg");

	
	public static Color ROJO=new Color(255,0,0);
	public static Color VERDE=new Color(0,255,0);
	public static Color NEGRO=new Color(0,0,0);
	public static Jugador[] Jugadores={Gaia,Jugador1};

	char[] ctrlChar= new char [4];
	
	Juego()
	{
		// Inicializa el objeto entorno, pero aun no lo inicia.
		entorno = new Entorno(this, "Juego", RES.x, RES.y);
		
		//entorno.setCursor(Normal);// Setea Cursor tipo flecha
		
		/// Por ahora esta desprolijo esto
	    
		
		//GENERAR GRAFICOS PRIMERO ACA

		entorno.iniciar();
		System.out.println("\nEntorno Iniciado");
		ctrlChar[0]=entorno.TECLA_ARRIBA;
		ctrlChar[1]=entorno.TECLA_ABAJO;
		ctrlChar[2]=entorno.TECLA_DERECHA;
		ctrlChar[3]=entorno.TECLA_IZQUIERDA;
				

	

		/* Es fundamental que reci�n al final del constructor de la clase Juego se 
		 * inicie el objeto entorno de la siguiente manera.
		 * Durante el juego, el m�todo tick() ser� ejecutado en cada instante y 
		 * por lo tanto es el m�todo m�s importante de esta clase. Aqu� se debe 
		 * actualizar el estado interno del juego para simular el paso del tiempo 
		 * (ver el enunciado del TP para mayor detalle).*/
}
		
	public void tick(){

		//Controles();
		juego();
		JugadorManager(Jugadores);
		
		}
	

	/*public void Controles(){
	    //Controles Jug 1
		Point Mouse=mouse(); //Carga ubicacion del mouse;
		
		if ((xp1!=Mouse.x)||(yp1!=Mouse.y)){//SOLO SI moviste el mouse(si es diferente a la ultima posicion registrada)
		
			//REALIZA ACCION
			}
		
		if ((entorno.clickPresionado(entorno.CLICK_IZQUIERDO))&Jugador1.click!=true){
			Jugador1.click=true;
			
			//LLAMAR A UNA FUNCION PARA ADMINISTRAR EL CLICK
			
			
		}
		else if ((!entorno.clickPresionado(entorno.CLICK_IZQUIERDO)&Jugador1.click==true)){Jugador1.click=false;}
		
		if ((entorno.estaPresionada(entorno.TECLA_ENTER))&Jugador1.click!=true){
			Jugador1.click=true;
		
			
		}
		else if ((!entorno.estaPresionada(entorno.TECLA_ENTER)&Jugador1.click==true)){Jugador1.click=false;}
		
		Point Cursor=Teclado();
		//Controles Jug 2
		
		

		
		xp1=Mouse.x;//Actualiza ubicacion del mouse en variable aux
		yp1=Mouse.y;
		
	}*/
	
	
	//cONTROLES TECLADO
	public Point Teclado(){
		int Desplazamiento=RES.x/10;
		Point Cursor=new Point(xp2,yp2); //
		
		if ((!entorno.estaPresionada(entorno.TECLA_ARRIBA)&Up)){Up=false;}
		if ((!entorno.estaPresionada(entorno.TECLA_ABAJO)&Down)){Down=false;}
		if ((!entorno.estaPresionada(entorno.TECLA_IZQUIERDA)&Left)){Left=false;}
		if ((!entorno.estaPresionada(entorno.TECLA_DERECHA)&Right)){Right=false;}
		
		if (entorno.estaPresionada(entorno.TECLA_ARRIBA)&!Up){

			Up=true;}
		
		if (entorno.estaPresionada(entorno.TECLA_ABAJO)&!Down){
			Down=true;}
		
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)&!Left){
			Adler.direccion+=2;
			
			Left=true;}
		
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)&!Right){
			Adler.direccion-=2;
			
			Right=true;}
		
		if (Cursor.x>RES.x){Cursor.x=RES.x;}//SI SE PASA queda en el borde
		if (Cursor.x<0){Cursor.x=0;}
		if (Cursor.y>RES.y){Cursor.y=RES.y;}
		if (Cursor.y<0){Cursor.y=0;}

		xp2=Cursor.x;
		yp2=Cursor.y;
		return Cursor;}
	
	public static Point mouse(){  //Recibe ubicacion del mouse, no deja que se vaya de la pantalla
		Point mouse=entorno.getMousePosition();
		
		if (mouse==null){mouse=new Point(x,y);}
	
		x=mouse.x;
		y=mouse.y;
		if (x>RES.x){x=RES.x;}
		if (y>RES.y){y=RES.y;}

		return mouse;}

	public void PantalladeCarga(){
		if (contTicks<tiempoDeCarga){
			entorno.dibujarRectangulo(0, 0,RES.x*2+50, RES.y*2.05, 0, NEGRO);
			entorno.cambiarFont("Ticks", 40, blanco);
			entorno.escribirTexto("Cargando...", RES.x/2-100, RES.y/2);
			entorno.dibujarRectangulo(contTicks*1.5, RES.y/2+50,contTicks*3,25, 0, VERDE);
		}
	}
	
	
	public void Fondo(Image Fondo  ){
		entorno.dibujarImagen(Fondo,RES.x/2,RES.y/2,0,1.026);//Fondo
		entorno.cambiarFont("Ticks", 20, blanco);
		entorno.escribirTexto(""+Integer.toString(contTicks/100),0, 20);
	}
	


	
	
	
	
	
	
	
	
	public static void JugadorManager(Jugador [] Jugadores){
		//MAnejo de los objetos de los jugadores
	}

	
	//////////////////////////////////////////////////////////////
	

	


	
	
	//Checkea si pasas el mouse por arriba de un objeto(Interseccion Circulo-point)
	public static boolean Objselect (Object objeto, Point mouse,int Sensitividad){ // Hipotenusa calcula distancia
		
		int cat1=(int)Math.round(Math.abs(objeto.x-mouse.x));
		int cat2=(int)Math.round(Math.abs(objeto.y-mouse.y));
		float Distancia=(float)Math.sqrt(((cat1)*(cat1))+((cat2)*(cat2)));
		if (Distancia<=(int)objeto.diametro+Sensitividad){return true;}
		return false;}
	
	

	public static boolean Hit(Object Pelota, Object Paletat){
			Rectangle A=new Rectangle ((int)Paletat.x,(int)Paletat.y,(int)Paletat.largo,(int)Paletat.ancho);
			Point PA1=new Point (A.x,A.y);
			Point PA2=new Point ((A.x+A.width),(A.y+A.height));
			float dim=Pelota.diametro;
			if ((Pelota.x>PA1.x-dim)&(Pelota.x<PA2.x-dim)&(Pelota.y>PA1.y-dim)&(Pelota.y<PA2.y-dim)){
				return true;}
			return false;}
		

	
	
	
	//Checkea interseccion Punto-rectangulo  ---Para menu OPCIONES
	public static boolean IntersecPointRect(Point puntoB, Rectangle A){
		Point PAx1=new Point (A.x,A.y);
		Point PAy2=new Point ((A.x+A.width),(A.y+A.height));
		
		if ((puntoB.x>PAx1.x)&(puntoB.x<PAy2.x)&(puntoB.y>PAx1.y)&(puntoB.y<PAy2.y)){
			return true;}
		return false;}

	public void Desplazamiento(){

	
		if (Adler.direccion>360){Adler.direccion=0;}
		else if(Adler.direccion<0){Adler.direccion=360;}
		Adler.velocx=(Math.cos(Adler.direccion)*Adler.veloc);
		Adler.velocy=(Math.sin(Adler.direccion)*Adler.veloc);
	}
//Genera una coordenada en pantalla que no se vaya de esta
	/*
	public static Point RandPointScreen(double diametro){// Genera un random del T de la pantalla
		
	
		
		Random ran = new Random();
		
		
		int ddiametro=(int)diametro;
		
		int maximoy=Juego.RES.y-(ddiametro*2);
		int minimoy=ddiametro*2;
		
		int maximox=Juego.RES.x-ddiametro*2;
		int minimox=(ddiametro*2);
		
		
		int x=ran.nextInt(maximox-minimox)+minimox;
		int y=ran.nextInt(maximoy-minimoy)+minimoy;

		
		return new Point(x,y);}
*/
	
public void juego(){
		
		contTicks+=1;
		Fondo(Fondo);  //METER LOS ARCHIVOS DE FONDO  O CUALQUIER OTRA IMAGEN DENTRO DEL PAQUETE YDESCOMENTAR CODIGO ARRIBA
		Teclado();
		Desplazamiento();
		Adler.x+=Adler.velocx;
		Adler.y+=Adler.velocy;
		//entorno.dibujarTriangulo(Adler.x, Adler.y,(int) Adler.diametro,(int) Adler.diametro/2,Herramientas.radianes(Adler.direccion), ROJO);
		entorno.dibujarTriangulo(Adler.x, Adler.y,(int) Adler.diametro,(int) Adler.diametro/2,Herramientas.radianes(300+Adler.direccion), ROJO);
		
		
		
		
		
		
		


		
					}

	public static void main(String[] args)
	{		
		
		Juego juego = new Juego();

	}
}
