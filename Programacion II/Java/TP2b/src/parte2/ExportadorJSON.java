package parte2;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExportadorJSON extends Exportador
{
	private Atributos atributos;
	private String nombre;
	private final String formato = ".json";

	public String getNombre() {
		return nombre;
	}

	public ExportadorJSON(String file) {
		this.nombre = file + this.formato;
	}

	@Override
	public void guardarCampos(Atributos atrs) {
		this.atributos = atrs;

	}

	@Override
	public void exportarArchivo(Iterable<? extends Exportable> objetos) {
		System.out.println("el nombre:" + this.nombre);
		path = Paths.get(this.getNombre());
		try {
			file = Files.newBufferedWriter(path, StandardCharsets.UTF_8);
			boolean seenFirst = false;
			printer = new PrintWriter(file);
			for (Exportable e : objetos) {
				this.atributos = e.extraerAtributos();
				// Imprimir la línea con los atributos.
				if (!seenFirst) {
					// Hay que escribir la apertura del arreglo.
					seenFirst = true;
					printer.print("[ ");
				}
				else {
					// Si no, la coma de separación.
					printer.print(", ");
				}
				printer.print("{");
				for (String k : this.atributos.keySet()) {
					printer.print(" \"" + k + "\": \"" + this.atributos.get(k)
					                + ", ");
				}
				printer.println("}");
				printer.println("]");

			}
			file.close();

		} catch (IOException e) {
			System.err.println("No se pudo realizar la copia");
		}
	}

	public String getFormato() {
		return formato;
	}

}
