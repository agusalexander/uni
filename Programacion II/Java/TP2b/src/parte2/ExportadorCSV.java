package parte2;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExportadorCSV extends Exportador
{
	private final String formato = ".csv";

	private Atributos atributos;

	public String getNombre() {
		return nombre;
	}

	public ExportadorCSV(String file) {
		this.nombre = file + this.formato;

	}

	@Override
	public void guardarCampos(Atributos atrs) {
		this.atributos = atrs;

	}

	@Override
	public void exportarArchivo(Iterable<? extends Exportable> objetos) {
		path = Paths.get(this.getNombre());
		try {
			file = Files.newBufferedWriter(path, StandardCharsets.UTF_8);
			printer = new PrintWriter(file);
			boolean seenFirst = false;
			for (Exportable e : objetos) {
				Atributos attrs = e.extraerAtributos();
				guardarCampos(attrs);
				if (!seenFirst) {
					seenFirst = true;
					for (String columna : this.atributos.keySet()) {
						printer.print(columna + ",");
					}
					printer.println();
				}
				// Imprimir la línea con los atributos.
				for (String val : this.atributos.values()) {
					printer.print(val + ",");
				}
				printer.println();
			}
			file.close();
		} catch (IOException e) {
			System.err.println("No se pudo realizar la copia");
		}
	}

	public String getFormato() {
		return formato;
	}

}
