package parte2;

import java.util.Iterator;

/**
 * Otra clase de ejemplo con varios atributos.
 */
public class Empleado implements Iterable<Empleado>, Exportable
{
	private String nombre;
	private String cuil;
	private int salario;

	public Empleado(String nombre, String cuil, int salario) {
		this.nombre = nombre;
		this.cuil = cuil;
		this.salario = salario;
	}

	public String getNombre() {
		return nombre;
	}

	public String getCuil() {
		return cuil;
	}

	public int getSalario() {
		return salario;
	}

	@Override
	public String toString() {
		return nombre + " " + cuil + " " + salario + " ";
	}

	@Override
	public Iterator<Empleado> iterator() {
		return this.iterator();
	}

	@Override
	public Atributos extraerAtributos() {
		Atributos attrs = new Atributos();
		attrs.put("nombre", this.getNombre());
		attrs.put("cuil", this.getCuil());
		attrs.put("salario", String.valueOf(this.getSalario()));
		return attrs;
	}
}
