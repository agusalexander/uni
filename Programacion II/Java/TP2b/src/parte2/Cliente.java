package parte2;

import java.util.Iterator;

/**
 * Una clase de ejemplo con varios atributos.
 */
public class Cliente implements Iterable<Cliente>, Exportable
{
	public enum Categoria {
		NORMAL,
		VIP,
	}

	private String nombre;
	private String cuit;
	private Categoria categoria;

	public Cliente(String nombre, String cuit, Categoria categoria) {
		this.nombre = nombre;
		this.cuit = cuit;
		this.categoria = categoria;
	}

	public String getNombre() {
		return nombre;
	}

	public String getCuit() {
		return cuit;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	@Override
	public String toString() {
		return nombre + " " + cuit + " " + categoria + " ";
	}

	@Override
	public Iterator<Cliente> iterator() {
		return this.iterator();
	}

	@Override
	public Atributos extraerAtributos() {
		Atributos attrs = new Atributos();
		attrs.put("nombre", this.getNombre());
		attrs.put("cuit", this.getCuit());
		attrs.put("categoria", this.getCategoria().toString());
		return attrs;
	}
}
