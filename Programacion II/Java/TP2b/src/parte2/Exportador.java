package parte2;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.nio.file.Path;

public abstract class Exportador
{
	protected String nombre;
	protected Path path;
	protected BufferedWriter file;
	protected PrintWriter printer;

	public abstract void guardarCampos(Atributos atrs);

	public abstract void exportarArchivo(Iterable<? extends Exportable> objetos);
}
