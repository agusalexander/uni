package parte1;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class main extends JFrame
{

	private JPanel contentPane;
	private JTextField Clave;
	private JTextField Elemento;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {

					main frame = new main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setResizable(false);
		DiccConjunto<String, String> dic = new DiccConjunto<String, String>();

		setTitle("Diccionario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 280);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		Clave = new JTextField();
		Clave.setBounds(134, 58, 146, 26);
		contentPane.add(Clave);
		Clave.setColumns(10);

		Elemento = new JTextField();
		Elemento.setBounds(134, 145, 146, 26);
		contentPane.add(Elemento);
		Elemento.setColumns(10);

		JLabel lblClave = new JLabel("Clave");
		lblClave.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblClave.setBounds(177, 34, 69, 20);
		contentPane.add(lblClave);

		JLabel lblElemento = new JLabel("Elemento");
		lblElemento.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblElemento.setBounds(163, 109, 171, 20);
		contentPane.add(lblElemento);

		JButton Buscar = new JButton("Buscar");
		Buscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String key = Clave.getText();

				if (!key.equals("")) {

					TuplaDic tup = new TuplaDic(key, null);

					if (dic.pertenece(key)) {
						String texto = dic.obtener(key).toString();
						if (texto != null) {
							Elemento.setText(texto);

						}
					}
					else {
						Elemento.setText("No se encuentra");
					}

				}

			}
		});
		Buscar.setBounds(314, 33, 99, 26);
		contentPane.add(Buscar);

		JButton Agregar = new JButton("Agregar");
		Agregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String key = Clave.getText();
				String elem = Elemento.getText();
				if (!(key.equals("") && elem.equals(""))) {
					dic.guardar(key, elem);
				}
				Clave.setText("");
				Elemento.setText("");

			}
		});
		Agregar.setBounds(314, 88, 99, 29);
		contentPane.add(Agregar);

		JButton Eliminar = new JButton("Eliminar");
		Eliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String key = Clave.getText();
				dic.eliminar(key);
				Clave.setText("");
				Elemento.setText("");
			}
		});
		Eliminar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		Eliminar.setBounds(314, 144, 99, 29);
		contentPane.add(Eliminar);

	}
}
