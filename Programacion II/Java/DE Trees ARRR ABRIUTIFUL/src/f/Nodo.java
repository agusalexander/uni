package f;

import java.util.LinkedList;

public class Nodo {
	public int Carga;
	public Nodo Izquierdo;
	public Nodo Derecho;
	int nivel;
	

	public Nodo (int load){
		this.Carga=load;

		}

	
	public void Agregar(Nodo N ){
		
		if(this.Izquierdo == null){
			this.Izquierdo=N;
			
		}
		else if(this.Derecho == null){
			this.Derecho=N;
		}
		
		else {
			
			if (this.Izquierdo.tieneVacios()){
				
				this.Izquierdo.Agregar(N);}
			
			else if ((this.Derecho.tieneVacios())){
				
				this.Derecho.Agregar(N);
				}
			
			else{
				this.Izquierdo.Agregar(N);
			}
			
			
		}
			
	}
	
	
	public int Buscar(Nodo nod){
		int rta=0;
		if (this.Carga==nod.Carga){
			rta=nod.Carga;
			System.out.print("Nodo Encontrado: "+nod);
			return rta;
		
		}
		else{ 
			if (this.Izquierdo!=null){
				rta=rta+this.Izquierdo.Buscar(nod);
			}
			if (this.Derecho!=null){
				rta=rta+this.Derecho.Buscar(nod);
			}
			
			}
		return rta;

	}
	
	public boolean tieneVacios(){
		
		if ((this.Izquierdo == null)||
				(this.Derecho == null)){
			
			return true;
		}
		return false;
	}
	
	
	public String toString(){
		String s=Integer.toString(this.Carga)+" ";
		return s;
	}	

	public void Imprimir(){
		System.out.print(this);
		
		if (this.Izquierdo!=null){
			System.out.print("(IZQ)");
			this.Izquierdo.Imprimir();
			
		}
		if(this.Derecho!=null){
			System.out.print("(DER)");
			this.Derecho.Imprimir();}
			
		}
	
	public void toList(LinkedList<Integer> lista){
		
		if (this.Izquierdo!=null){
			lista.add(this.Izquierdo.Carga);
		}
		if (this.Derecho!=null){
			lista.add(this.Derecho.Carga);
		}
		
		
	
		if(this.Izquierdo!=null){
			if(this.Izquierdo.tieneHijos()){
			this.Izquierdo.toList(lista);
			}
		}
		
		if(this.Derecho!=null){
		if((this.Derecho.tieneHijos())&&this.Derecho!=null){
			this.Derecho.toList(lista);
			}
		}
		
	}
	
	
	
public boolean tieneHijos(){
		
		if (this.Izquierdo!=null||this.Derecho!=null){
			return true;
		}
		
		return false;
		
	}
	

	
	
	public void Eliminar(int carga){
		
		if (this.Izquierdo!=null){
			
			if(this.Izquierdo.Carga==carga){
				
				this.Izquierdo=ReorganizarHijos(this.Izquierdo);
				
				return;
			}
			else{
				this.Izquierdo.Eliminar(carga);}
		}
		
		
		if (this.Derecho!=null){
			
			if(this.Derecho.Carga==carga){
				
				this.Derecho=ReorganizarHijos(this.Derecho);
				
				return;	
			}
			else{
				this.Derecho.Eliminar(carga);}
		}
		
	}
	
	
public static Nodo ReorganizarHijos(Nodo este){

	if(este.tieneHijosn()==0){    //SI NO TIENE
		este=null;
		
	}
	
	else if(este.tieneHijosn()==1){ //SI TIENE A LA IZQ
		este=este.Izquierdo;
	}
	
			
	else if(este.tieneHijosn()==2){ // SI TIENE A LA DER
		este=este.Derecho;
	}
	else{
		Nodo BKP=este.Derecho;		//SI TIENE EN LOS DOS
		este=este.Izquierdo;
		Nodo ProxHoja=este.proximaHoja();
		ProxHoja.Derecho=BKP;
		}
	
	return este;
	
}	
	
		

public Nodo proximaHoja(){
	if ((this.Izquierdo==null)&&(this.Derecho==null)){
		return this;
	}
	else{
		return this.Derecho.proximaHoja();
	}
	
	
}	
	
	
public int tieneHijosn(){
	if((this.Izquierdo!=null)&&(this.Derecho!=null)){
		return 3;
		
	}
	if(this.Izquierdo!=null&&this.Derecho==null){
		return 1;
	}
	if(this.Izquierdo==null&&this.Derecho!=null){
		return 2;
	}
	return 0;
	
}		
		
		
	
	

	public void Imprimirbeta(){
		

		if(this.Izquierdo != null){
			System.out.println(this);
			
		}
		if(this.Derecho != null){
			System.out.println(this);
		}
		
		
			if(this.Izquierdo!=null){
				if (!this.Izquierdo.tieneVacios()){
				
					this.Izquierdo.Imprimirbeta();}}
			
			if(this.Derecho!=null){
				if ((!this.Derecho.tieneVacios())){
				
					this.Izquierdo.Imprimirbeta();
				}}
			
			else{
				return;
			}
			
			
		}



			
	
			
	
	
	
	
}
