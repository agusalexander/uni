import java.util.LinkedList;

import f.Arbol;
import f.Nodo;

public class NodoBin {


	int Carga;
	NodoBin Izquierdo,Derecho;

	public NodoBin(int load) {
		this.Carga=load;
	}

	
	public void Agregar(NodoBin N ){
		
		
		if(this.Carga>=N.Carga){
			if(this.Izquierdo==null){
				this.Izquierdo=N;
			}
			else{
				this.Izquierdo.Agregar(N);
				}
			}
	
		else if(this.Carga<N.Carga){
			if (this.Derecho==null){
				this.Derecho=N;
				}
			else{
				this.Derecho.Agregar(N);
				
				}
			}
	}
	
	public void toList(LinkedList<Integer> lista){
		
		if (this.Izquierdo!=null){
			lista.add(this.Izquierdo.Carga);
		}
		if (this.Derecho!=null){
			lista.add(this.Derecho.Carga);
		}
		
		
	
		if(this.Izquierdo!=null){
			if(this.Izquierdo.tieneHijos()){
			this.Izquierdo.toList(lista);
			}
		}
		
		if(this.Derecho!=null){
		if((this.Derecho.tieneHijos())&&this.Derecho!=null){
			this.Derecho.toList(lista);
			}
		}
		
	}
	
	
	public boolean tieneHijos(){
		
		if (this.Izquierdo!=null||this.Derecho!=null){
			return true;
		}
		
		return false;
		
	}

	public int Altura(){
		int izq=0;
		int der=0;
		if(this.Izquierdo!=null){
			
			izq= 1+this.Izquierdo.Altura();
		
			}
		if(this.Derecho!=null){
			der=1+this.Derecho.Altura();
		}
		
		return (izq>der)? izq:der;
	}
	
	public int CantNodos(){
		int x=0;
		if(this.Izquierdo!=null){
			x= x+1+this.Izquierdo.CantNodos();
		}
		if(this.Derecho!=null){
			x=x+ 1+this.Derecho.CantNodos();
		}
		return x;
		
		
	}
	
	
	
	public int get(int i){
		
		
		if(i==0){
			System.out.println(this.Carga);
		}
		
		if(this.Izquierdo!=null){
			i=-1+this.Izquierdo.get( i);
		}
		if(this.Derecho!=null){
			i=-1+this.Derecho.get( i);
		}
		
		
		return i;
		
		
	}

	public boolean Buscar(NodoBin nod){
		boolean rta=false;
		if(this.Carga==nod.Carga){
			return true;
			
		}
		
		else if ((this.Carga>nod.Carga)&&(this.Izquierdo!=null)){
				rta=rta||this.Izquierdo.Buscar(nod);
			}
		else if((this.Carga<nod.Carga)&&(this.Derecho!=null)){
			rta=rta||this.Derecho.Buscar(nod);
		}
		
		return rta;
			
			
		}
		

	public String toString(){
		String s=Integer.toString(this.Carga)+" ";
		return s;
	}	

	public void Imprimir(){
		System.out.print(this);
		
		if (this.Izquierdo!=null){
			System.out.print("(IZQ)");
			this.Izquierdo.Imprimir();
			
		}
		if(this.Derecho!=null){
			System.out.print("(DER)");
			this.Derecho.Imprimir();
			
		}
		
		
		
		
	}
	

	




	
	
	
	
	

			
	
			
	
	
	
	
}
