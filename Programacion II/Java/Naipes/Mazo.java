
public class Mazo {
	//Este es el TAD soporte para el PGG. Mazo seria una representacion general de mazos de cartas
	//dicho esto, PGG es un mazo de cartas especifico que se emplea sobre este TAD.
	//este TAD se emplea sobre una lista doblemente enlazada creada previamente por el grupo (ver ListaInt).
	ListaInt mazo;
	
	public Mazo(){
		this.mazo = new ListaInt ();
	}
	//agrega una carta al tope 
	public void agregarArriba(Integer num){
		if(num < 0)
			throw new RuntimeException ("No pueden existir cartas negativas en los mazos");
		this.mazo.agregarAdelante(num);
	}
	//quita la primer carta del mazo.
	public Integer quitarCarta(){
		return this.mazo.quitarPrimero();
	}
	
	//agrega una carta ebajo del mazo.
	public void agregarAbajo(Integer num){
		this.mazo.agregarAtras(num);
	}

}
