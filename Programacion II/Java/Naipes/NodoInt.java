
public class NodoInt {
	//Esta clase define un nodo que sera la base de la lista empleada en el TAD mazo (del TAD PGG)
	//Cada nodo se enlaza con si siguiente y su anterior para que la lista sea doblemente enlazada.s
	private int elemento;
	private NodoInt siguiente;
	private NodoInt anterior;
	
	
	public NodoInt(int elemento){
		this.elemento=elemento;
		this.siguiente=null;
		this.anterior=null;
	}
	
	public int getElemento() {
		return elemento;
	}

	public void setElemento(int elemento) {
		this.elemento = elemento;
	}

	public NodoInt getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoInt siguiente) {
		this.siguiente = siguiente;
	}

	public NodoInt getAnterior() {
		return anterior;
	}


	public void setAnterior(NodoInt anterior) {
		this.anterior = anterior;
	}

	public String toString(){
		return elemento+"";
	}
	
	
	
	
	
}
