package parte1;

public class DiccConjunto<K extends Comparable<K>, V> implements Diccionario<K, V>
{
	/**
	 * Conjunto privado que DiccConjunto usa para almacenar sus parejas
	 * (clave, significado).
	 */

	private Conjunto<TuplaDic<K, V>> elementos;
	private int size = 0;

	public DiccConjunto() {
		elementos = new Conjunto<>();
	}

	@Override
	public void guardar(K clave, V valor) {
		/*
		 * if (!(clave instanceof String && valor instanceof String)) {
		 * throw new RuntimeException(
		 * "La clave y el valor deben ser tipo String"); }
		 */

		TuplaDic<K, V> nuevo = new TuplaDic<K, V>(clave, valor);

		if (!elementos.pertenece(nuevo)) {
			elementos.agregar(nuevo);
			size++;

			// Si ya pertenece no se incrementa
		}
		else if (elementos.pertenece(nuevo)) {
			eliminar(clave);
			elementos.agregar(nuevo);
			size++;
			// Si ya pertenece no se incrementa
		}

	}

	@Override
	public V obtener(K clave) {
		if (size <= 0) {
			return null;
		}
		TuplaDic<K, V> tup = new TuplaDic<K, V>(clave, null);

		tup = elementos.recuperar(tup);
		if (tup != null) {

			return tup.getE2();
		}

		return null;

	}

	@Override
	public boolean pertenece(K clave) {

		boolean ret = false;
		if (size > 0) {
			TuplaDic<K, V> tup = new TuplaDic<K, V>(clave, null);
			if (elementos.pertenece(tup)) {
				ret = true;
			}
		}
		return ret;
	}

	@Override
	public void eliminar(K clave) {
		if (size <= 0) {
			return;
		}
		TuplaDic<K, V> tup = new TuplaDic<K, V>(clave, null);
		if (elementos.pertenece(tup)) {
			elementos.quitar(tup);
			size--;

		}

	}

	@Override
	public int tamaño() {
		return size;
	}

	public static void main(String args[]) {
		Diccionario<String, Integer> d = new DiccConjunto<>();
		System.out.println(d.pertenece("Juan"));
		System.out.println(d.obtener("Juan"));
	}
}
