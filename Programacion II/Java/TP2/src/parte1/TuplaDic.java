package parte1;

/**
 * Tupla inmutable de dos elementos, para uso con DiccConjunto.
 *
 * equals() y compareTo() solo se fijan en el primer elemento.
 */

public class TuplaDic<T1 extends Comparable<T1>, T2> implements Comparable<TuplaDic<T1, T2>>
{

	private final T1 e1;
	private final T2 e2;

	public TuplaDic(T1 a, T2 b) {
		e1 = a;
		e2 = b;
	}

	public T1 getE1() {
		return e1;
	}

	public T2 getE2() {
		return e2;
	}

	/*
	 * IMPLEMENTAR LOS SIGUIENTES MÉTODOS.
	 */

	@Override
	public String toString() {

		return "(" + this.e1 + "," + this.e2 + ")";
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}

		if (obj instanceof TuplaDic) {

			TuplaDic<T1, T2> nueva = (TuplaDic<T1, T2>) obj;

			if (nueva.e1 != null && this.e1 != null) {
				if (nueva.e1.equals(this.e1)) {

					return true;
				}
			}
			if (nueva.e1 == null && this.e1 == null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int compareTo(TuplaDic<T1, T2> tupla) {
		if (tupla == null) {
			throw new RuntimeException("No se puede comparar nulls");
		}
		if (this.e1 == null || tupla.e1 == null) {
			if (this.e1 == null && tupla.e1 == null) {
				return 0;
			}
			else if (this.e1 == null && tupla.e1 != null) {
				return -1;
			}
			else {
				return 1;
			}
		}

		return this.e1.compareTo(tupla.e1);

	}
}
