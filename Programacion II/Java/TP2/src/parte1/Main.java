package parte1;

public class Main
{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DiccConjunto<Integer, String> dic = new DiccConjunto();

		TuplaDic a = new TuplaDic(null, "Perro");
		TuplaDic b = new TuplaDic(3, "Perro");

		System.out.println(a.compareTo(b));
		System.out.println(a.equals(b));
		dic.guardar(null, null);
		dic.guardar(1, "Perro");
		dic.guardar(1, "Gato");

		System.out.println(dic.obtener(1));
		System.out.println(dic.pertenece(null));

	}

}
