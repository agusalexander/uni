package parte2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public abstract class Exportador
{
	protected String nombre;
	protected Path path;
	protected BufferedWriter file;
	protected PrintWriter printer;
	private String formato;

	public Exportador(String n) {

		path = Paths.get(n + getFormato());
		try {
			file = Files.newBufferedWriter(path, StandardCharsets.UTF_8);
			printer = new PrintWriter(file);
		} catch (IOException e) {
			System.err.println("No se pudo realizar la copia");
		}
	}

	public abstract void exportarArchivo(List<? extends Exportable> objetos);

	public String getFormato() {

		return formato;
	}
}
