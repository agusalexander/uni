package parte2;

public interface Exportable
{
	/**
	 * Devuelve los atributos de la instancia en un diccionario.
	 *
	 * En esta versión simplificada, los valores siempre son strings
	 * (ver clase Atributos).
	 */
	Atributos extraerAtributos();

}
