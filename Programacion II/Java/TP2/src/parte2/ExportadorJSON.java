package parte2;

import java.io.IOException;
import java.util.List;

public class ExportadorJSON extends Exportador
{

	private final String formato = ".json";

	public String getNombre() {
		return nombre;
	}

	public ExportadorJSON(String file) {
		super(file);
	}

	@Override
	public void exportarArchivo(List<? extends Exportable> objetos) {

		try {

			boolean seenFirst = false;

			for (Exportable e : objetos) {
				Atributos atts = e.extraerAtributos();
				// Imprimir la línea con los atributos.
				if (!seenFirst) {
					// Hay que escribir la apertura del arreglo.
					seenFirst = true;
					printer.print("[ ");
				}
				else {
					// Si no, la coma de separación.
					printer.print(", ");
				}
				printer.print("{");
				for (String k : atts.keySet()) {
					printer.print(" \"" + k + "\": \"" + atts.get(k)
					              + ", ");
				}
				printer.println("}");
				printer.println("]");

			}
			file.close();

		} catch (IOException e) {
			System.err.println("No se pudo realizar la copia");
		}
	}

	@Override
	public String getFormato() {
		return formato;
	}
}
