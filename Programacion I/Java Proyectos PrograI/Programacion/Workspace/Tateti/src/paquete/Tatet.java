package paquete;
import java.awt.Point;
import java.util.Scanner;
public class Tatet {
	char[][] matrix;
	Point[] movimiento1;
	Point[] movimiento2;
	int rondas, puntos1,puntos2;
	
	public Tatet(){
		this.matrix= new char[3][3]; 
		for (int x=0; x<3;x++){for(int y=0; y<3;y++){this.matrix[x][y]=' ';}}
		//Crea matriz vacia
		this.rondas=0;
		this.puntos1=0;
		this.puntos2=0;
		this.movimiento1=new Point[5];
		this.movimiento2=new Point[5];
	}
	public static char winCheck(Tatet partida){
		char result='n';
		if (partida.rondas<4){return result;}//imposible ganar con menos de 3 movimientos
		
		else{
			for (int x=0;x<3;x++){				//Desplazamiento horizontal (en este caso y)
					if ((partida.matrix[x][0]!=' ')&((partida.matrix[x][0]==partida.matrix[x][1])&(partida.matrix[x][1]==partida.matrix[x][2]))){
						result=partida.matrix[x][0];return result;}
													//Vertical(x)
					else if ((partida.matrix[0][x]!=' ')&((partida.matrix[0][x]==partida.matrix[1][x])&(partida.matrix[1][x]==partida.matrix[2][x]))){
						result=partida.matrix[0][x];return result;}
													//159
					else if ((partida.matrix[0][0]!=' ')&((partida.matrix[0][0]==partida.matrix[1][1])&(partida.matrix[1][1]==partida.matrix[2][2]))){
						result=partida.matrix[0][0];return result;}
													//357
					else if ((partida.matrix[0][2]!=' ')&((partida.matrix[0][2]==partida.matrix[1][1])&(partida.matrix[1][1]==partida.matrix[2][0]))){
						result=partida.matrix[0][2];return result;}}}
		return result;}
		
	
	

	public Tatet(int puntos1 , int puntos2){ //Juega una nueva partida sin perder el puntaje anterior
		this.matrix= new char[3][3]; 
		for (int x=0; x<=3;x++){for(int y=0; y<=3;y++){this.matrix[x][y]=' ';}}
		this.rondas=0;
		this.puntos1=0;
		this.puntos2=0;}
	
	public static int scan(){
		Scanner nuevo= new Scanner(System.in);
		int n=nuevo.nextInt();
		//nuevo.close()
		return n;}
	
	public static void ingresaJugada(Tatet partida, boolean jugador){
		int i=0;
		
		Point punto=ToPoint(scan());
		
		if (partida.matrix[punto.x][punto.y]!=' '){ // si el casillero esta ocupado
			while (partida.matrix[punto.x][punto.y]!=' '){ //para que no lo buguee
			System.out.println("Casillero ocupado, intente de nuevo:");
			punto=ToPoint(scan());}}
		
		if (jugador==true){
			Movimiento(punto,partida,'X');
			while (partida.movimiento1[i]!=null&i<5){
			i++;}
			partida.movimiento1[i]=punto;}
		
		else if (jugador==false){
			Movimiento(punto,partida,'O');
			while (partida.movimiento2[i]!=null&i<5){
			i++;}
			partida.movimiento2[i]=punto;}}
			
		
		
		
			
		
		
	public static Point ToPoint(int n){
		if (n>9||n<1){n=1;}//para que el usuario no se pase de vivo
		
		int x=(n%3)-1;
		int y=(n/3);

		if (n%3==0){x=2;y-=1;}
		if (x<0){x=0;}
		if (y<0){y=2;}
		Point punto= new Point(y,x);
		return punto;}
	
	public static void ImprimeEspacios(boolean cantidad){
		if (cantidad==true){System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");}
		else if (cantidad==false){System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");}}
	
	public static void GUI(Tatet Juego){
		
		System.out.println("O="+Juego.puntos1+"   "+Juego.puntos2+"=X\n");
		System.out.println("|"+Juego.matrix[2][0]+"||"+Juego.matrix[2][1]+"||"+Juego.matrix[2][2]+"|");
		System.out.println("|"+Juego.matrix[1][0]+"||"+Juego.matrix[1][1]+"||"+Juego.matrix[1][2]+"|");
		System.out.println("|"+Juego.matrix[0][0]+"||"+Juego.matrix[0][1]+"||"+Juego.matrix[0][2]+"|");
		System.out.println("Ingrese un numero del 1 al 9:");
	}
	
	public static void Movimiento(Point punto,Tatet partida,char XO){
		partida.matrix[punto.x][punto.y]=XO;}
	
}
