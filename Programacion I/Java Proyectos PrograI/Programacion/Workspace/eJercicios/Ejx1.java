package eJercicios;
import java.util.Scanner;

public class Ejx1 {
	static int escanearInt(){ 
		Scanner objetoscan= new Scanner(System.in); 
		int escaneado= objetoscan.nextInt();
		return escaneado;}
	static int sumaEnteros(int n){
		int acum=0;
		for (int i = 1; i<=n;i++){acum=acum+i;}
		return acum;}


	public static void main(String[] args) {
	System.out.println("Ingrese un entero a sumar");
	int n=escanearInt();
	System.out.println("La suma de los numeros hasta "+n+" es igual a: "+sumaEnteros(n));
	 	
	}

}
