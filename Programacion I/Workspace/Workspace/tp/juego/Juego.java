package juego;


import entorno.*;


public class Juego extends InterfaceJuego
{
	private Entorno entorno;

	Juego()
	{
		// Inicializa el objeto entorno, pero aun no lo inicia.
		entorno = new Entorno(this, "Guerra de Hormigas - Versi�n 0.01", 1000, 600);
		
		
		
		
		
		
		/* 
		 * Es fundamental que reci�n al final del constructor de la clase Juego se 
		 * inicie el objeto entorno de la siguiente manera.
		 */
		entorno.iniciar();
	}

	/*
	 * Durante el juego, el m�todo tick() ser� ejecutado en cada instante y 
	 * por lo tanto es el m�todo m�s importante de esta clase. Aqu� se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{

	
	}

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{		
		Juego juego = new Juego();
	}
}
