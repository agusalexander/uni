package paquete;
import java.util.Random; 
import java.util.Scanner;

public class jugador {
	int puntos;
	int movimiento;
	int vidas;
	int prevmov;
	boolean ganoanterior;
	String nombre;
	
	public jugador(String nombre1){
		this.ganoanterior=false;
		this.puntos=0;
		this.movimiento=3;
		this.prevmov=random(2);
		this.nombre=nombre1;
		this.vidas=3;}
	
	public static void gano(jugador jugador){jugador.puntos+=10;jugador.ganoanterior=true;
	System.out.println("      "+jugador.nombre+ " wins");}
	
	public static void perdio(jugador jugador){jugador.vidas-=1;jugador.ganoanterior=false;}
	
	public static void juego(jugador jugador1,jugador jugador2){
		if (jugador1.movimiento!=jugador2.movimiento){
			if (java.lang.Math.abs(jugador1.movimiento-jugador2.movimiento)>1){ // si el movimiento no es proximo entonces el mayor le gana el menor
				if (jugador1.movimiento>jugador2.movimiento){gano(jugador2);perdio(jugador1);}
				else if (jugador1.movimiento<jugador2.movimiento){gano(jugador1);perdio(jugador2);}}
			else{
				if (jugador1.movimiento>jugador2.movimiento){gano(jugador1);perdio(jugador2);}
				else if (jugador1.movimiento<jugador2.movimiento){gano(jugador2);perdio(jugador1);}}}
		else{System.out.println("EMPATE");jugador1.ganoanterior=true;jugador2.ganoanterior=true;}}
	

	
	public static void escanearInt(jugador usuario){

		usuario.prevmov=usuario.movimiento;
		Scanner nuevo= new Scanner(System.in);
		int n=(nuevo.nextInt())-1;
		usuario.movimiento=n;}
	
	public static void pantalla(int turno,jugador jugador2, jugador jugador1){
		String[] ppt= new String[4];
		ppt[0]="Piedra";
		ppt[1]="Papel";
		ppt[2]="Tijera";
		ppt[3]="";
		
	    System.out.println("              PPT           Turnos= "+turno);
		System.out.println(jugador1.nombre+": "+jugador1.vidas+"                     Puntos:"+jugador1.puntos);
		System.out.println(jugador2.nombre+": "+jugador2.vidas+"                     PC:"+jugador2.puntos);
		System.out.println("--------------(1)Piedra----------------");
		System.out.println("--------------(2)Papel-----------------");
		System.out.println("--------------(3)Tijera----------------");
		System.out.println(jugador1.nombre +": "+ppt[jugador1.movimiento]);
		System.out.println(jugador2.nombre +": "+ppt[jugador2.movimiento]);
		}
	

	
	
	public static void AI(jugador pc, jugador usuario){
		if (pc.movimiento==3){pc.movimiento=random(2);}
		
		int desicion = random(2);
		
		if (usuario.movimiento==usuario.prevmov){desicion=0;}//Para evitar que te tiren simpre el mismo mov
		
		if (pc.ganoanterior==true){
			if (desicion==1){pc.movimiento=usuario.prevmov;System.out.println(pc.movimiento);}
			else if (desicion==0){
				int randomInt = random(3);
				pc.movimiento=randomInt;}}
		else if (pc.ganoanterior==false){
			if (desicion==0){pc.movimiento=pc.prevmov;}
			else if (desicion==1){int randomInt = random(3);pc.movimiento=randomInt;}}
		pc.prevmov=pc.movimiento;}
	
	
	public static int random(int tope){
		Random randomnum = new Random();
		int desicion = randomnum.nextInt(tope);
		return desicion;}


}


