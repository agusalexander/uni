package eJercicios;
import java.util.Scanner;

public class Cantidad {
	static int cantidadVocales(String cadena){
		int acum=0;
		for (int i=0; i<cadena.length();i++){
			char letra=cadena.charAt(i);
			if (letra=='a' || letra=='e'|| letra=='i'||letra=='o'|| letra=='u'){acum++;}}
		return acum;}
	
	static int cantidad(String cadena){
		if (cadena.equals("")){return 0;}
		else{return 1+cantidad(cadena.substring(1));}}
	
	
	static String escanearString(){ 
		Scanner objetoscan= new Scanner(System.in); 
		String escaneado= objetoscan.nextLine();
		return escaneado;}
	
	
	
	public static void main(String[] args) {
	
		System.out.println("Ingrese una palabra para contar sus vocales:\n");
		String palabra=escanearString();
		System.out.println("La cantidad de vocales de la palabra:"+palabra+ " es= "+ cantidadVocales(palabra));
		System.out.println("La cantidad de letras de la palabra:"+palabra+ " es= "+ cantidad(palabra));
		
		
		 
}
	}
