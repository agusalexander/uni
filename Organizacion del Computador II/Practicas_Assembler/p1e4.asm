%include "io.inc"

section .data
N1 db 00001010b ;10
N2 db 00001100b ;12
N3 dw 00010h ;16
N4 dw 00020h ;32
N5 dd 00000001b ; 1
N6 dd 00000010b ; 2
N7 dq 0fffffffffffffff1h
N8 dq 0ffffffffffffffffh

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
    mov al,[N1]
    mov bl,[N2]
    cmp al,bl
    ja ejercicio1
continue2:
    mov ax,[N3]
    mov bx,[N4]
    cmp ax,bx
    jz ejercicio2
continue3:
    mov eax,[N5]
    mov ebx,[N6]
    add eax,ebx
    jo ejercicio3
continue4:
    mov eax,[N5]
    cmp eax,0
    jb ejercicio4
    PRINT_STRING 'Es Positivo'
    NEWLINE
continue5:
    mov eax,[N7]
    mov ebx,[N8]
    mov ecx,[N7+4]
    mov edx,[N8+4]
    add eax,ebx
    adc ecx,edx
    jc ejercicio5
continue6:
    mov eax,[N5]
    mov ebx,[N6]
    add eax,ebx
    jp ejercicio6
    jmp exit
       
ejercicio1:
    PRINT_STRING 'Es Mayor'
    NEWLINE
    jmp continue2    
ejercicio2:
    PRINT_STRING 'Es cero'
    NEWLINE
    jmp continue3
ejercicio3:
    PRINT_STRING 'Hay desbordamiento'
    NEWLINE
    jmp continue4
ejercicio4:
    PRINT_STRING 'Es negativo'
    NEWLINE
    jmp continue5    
ejercicio5:
    PRINT_STRING 'Produjo acarreo'
    NEWLINE
    jmp continue6
ejercicio6:
    PRINT_STRING 'Es par'          
    NEWLINE
exit:
    xor eax, eax
    ret