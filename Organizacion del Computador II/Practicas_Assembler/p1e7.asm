%include "io.inc"

section .data

sec db 1,2,3,250,128,12,96,11111110b,100,2,0


section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
    
    mov al,[sec]
    mov bl,[sec]
    mov ecx,0
    
ciclo:
    cmp al,0
    jne sigo
    PRINT_UDEC 1, bl
    jmp exit
    

sigo:
    cmp al,bl     
    ja esMayor
    INC ecx
    mov al,[sec+ecx]
    jmp ciclo
    
esMayor:
    mov bl,al
    INC ecx
    mov al,[sec+ecx]
    jmp ciclo
    
exit:    
    xor eax, eax
    ret