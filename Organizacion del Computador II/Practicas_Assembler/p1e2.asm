%include "io.inc"

section .data
N1 db 00001010b ;10
N2 db 00001100b ;12
N3 dw 00010h ;16
N4 dw 00020h ;32
N5 dd 00000001b ; 1
N6 dd 00000010b ; 2
N7 dq 100000d
N8 dq 12312312d

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
    mov al,[N1]
    mov bl,[N2]
    add al,bl
    PRINT_DEC 1, al
    PRINT_STRING ' Suma de N1 y N2'
    NEWLINE
    and al,00000000b
    and bl,00000000b
    mov ax, [N3]
    mov bx, [N4]
    sub ax,bx
    PRINT_DEC 2, ax
    PRINT_STRING ' Resta de N3 y N4'
    NEWLINE
    and ax,0000000000000000b
    and bx,0000000000000000b
    mov al, [N2]
    mov bl, [N1]
    mul bl
    PRINT_DEC 2, ax 
    PRINT_STRING' Multiplicacion de N1 y N2'
    NEWLINE
    and al,00000000b
    and bl,00000000b
    and ax,0000000000000000b
    mov ax,[N4]
    mov bl,[N3]
    div bl
    PRINT_DEC 1, al 
    PRINT_STRING' Division de N3/N4'
    NEWLINE
    PRINT_DEC 1, ah 
    PRINT_STRING' Resto de la division de N3/N4'
    NEWLINE
    and bl,00000000b
    and ax,0000000000000000b
    mov eax,[N5]
    mov ebx,[N6]
    add eax,ebx
    PRINT_DEC 4, eax 
    PRINT_STRING' Suma de N5 y N6'
    NEWLINE
    and eax,00000000000000000000000000000000b
    and ebx,00000000000000000000000000000000b
    ;Faltan ejercicos f) y g)
    xor eax, eax
    ret