%include "io.inc"

section .data

reg dd 11111111111111111111011111111111b

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
    
    pushf
    mov eax,esp
    and eax,[reg]
    mov esp,eax
    popf
    
    PRINT_UDEC 4,eax
    
    xor eax, eax
    ret