%include "io.inc"

section .data

n1 dw 40,16,33,11,8,0

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here

    mov ax,[n1]
    mov bl, 8
    mov ecx,0

ciclo:
    cmp ax,0
    jne sigo
    jmp exit  
      
sigo:      
    div bl
    cmp ah,0
    je esMultiplo
    mov ax,[n1+ecx*2]
    PRINT_UDEC 2, ax
    PRINT_STRING ' No es multiplo de 8'
    NEWLINE
    INC ecx
    mov ax,[n1+ecx*2]
    jmp ciclo
esMultiplo:
    mov ax,[n1+ecx*2]
    PRINT_UDEC 2, ax
    PRINT_STRING ' Es multiplo de 8'
    NEWLINE
    INC ecx
    mov ax,[n1+ecx*2]
    jmp ciclo
exit:    
    xor eax, eax
    ret