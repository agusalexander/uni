%include "io.inc"

section .data
F1 dd -0.0

section .text
global CMAIN
CMAIN:
    ;write your code here
    mov eax,[F1]    
    and eax, 01111111111111111111111111111111b
    cmp eax,0
    jz mensaje
    PRINT_STRING 'No es igual a 0'
    jmp exit
mensaje:
    PRINT_STRING 'Si soy 0'    
    jmp exit
exit:    
    xor eax, eax
    ret