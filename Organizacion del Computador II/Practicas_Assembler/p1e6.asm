%include "io.inc"

section .data

C db 'Hola Mundo',0

section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    ;write your code here
    mov al,[C]
    mov ebx,0
ciclo:
    cmp al,0
    jne sigo
    PRINT_DEC 1, ebx
    jmp exit
    
sigo:
    INC ebx
    mov al, [C+ebx]
    jmp ciclo    

exit:        
    xor eax, eax
    ret