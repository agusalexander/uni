%include "io.inc"

global CMAIN
extern _printf
section .data

MENSAJE db "Hola Mundo!",10,0

section .text
CMAIN:
    ;write your code here
    PUSH MENSAJE
    CALL _printf
    add esp,4
    jmp final
final: 
    xor eax, eax   
    ret