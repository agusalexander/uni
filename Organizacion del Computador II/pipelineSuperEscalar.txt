﻿Riesgo de datos
En un pipeline escalar estas si o si van a producir stalls, en cambio en superescalar va a poder aprovecharse en alguna medida las isntrucciones que no son independientes pero va a ver que garantizar que el orden sea el correcto x ej RAW o WAW
        ◦ Escribo un registro y acontinuacion leo su resultado, siempre que el pipeline detecte esa dependencia sabe que hay un riesgo en el pipeline. Entonces debe esperar que se ejecute la anterior.
        ◦ En el superescalar puede atenuarse porque puede hacer otras cosas mientras espera. 
        ◦ No hay forma de eliminar esta dependencia porque si o si debo ejecutar la escritura luego de haber leido.
    • RAW (read after write): 
    • WAW (Write after write):

Loop unrolling:

    • Replicar el cuerpo del ciclo para permitir el solapamiento de las instrucciones
    • Las iteraciones deben ser independientes
    • Usar registros para no agregar dependencias
    • Elimina comparaciones y bifurcaciones
    • preserva dependencias para que el resultado sea consistente
    • Planificacion estatica (compilador)

Ejecutar mas de un ciclo a la vez si es que no hay dependencia entre ellos x ej

for(int i; i< sarasa.lenght; i=i-1){
	sarasa[i]=i+constante
}
se puede desenrrollar y ejecutar en paralelo.

Prediccion de saltos

    • Estatica
        ◦ Saltos demorados
        ◦ Prediction taken
        ◦ Prediction not taken
    • Dinamica
        ◦  Branch prediction buffer
            ▪ Usa una tabla con parte de la direccion de la instruccion de salto y un bit que indica si se tomo o no
            ▪ Cada salto actualiza el bit en la tabla
            ▪ Inpresiso
            ▪ Se puede mejorar usando 2 bitspara predecir el salto


 
