-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: registrocivil
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `DNI` int(11) NOT NULL,
  `nombrePer` char(15) NOT NULL,
  `apellidoPero` char(15) NOT NULL,
  `fecha_nacim` date NOT NULL,
  `pesoPer` int(11) NOT NULL,
  `alturaPer` double NOT NULL,
  `sexo` char(10) NOT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (25647987,'Camila','Alvarez','1985-03-15',85,1.8,'Femenino'),(26157256,'Roberto','Suarez','1978-06-15',90,1.88,'Masculino'),(26741852,'Juan','Martinez','1975-11-24',76,1.76,' Masculino'),(26789456,'Vanesa','Moreno','1987-05-25',80,1.75,'Femenino'),(30147856,'Camila','Suarez','2000-11-06',75,1.7,'Femenino'),(30985746,'Camila','Marttinez','1980-08-24',85,1.85,'Femenino'),(34258741,'Jorge','Lopez','1995-03-02',78,1.81,'Masculino'),(34892176,'Agustin','Gonzalez','1992-12-28',75,1.84,'Masculino'),(34987321,'Rodrigo','Argarañaz','1980-12-31',88,1.9,'Masculino'),(39123487,'Alberto','Suarez','2000-11-06',80,1.85,'Masculino'),(39145789,'Roberto','Argarañaz','2000-12-31',80,1.75,'Masculino'),(39147852,'Camila','Martinez','2000-12-01',87,1.86,'Femenino');
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-17 13:29:10
