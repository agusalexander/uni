-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: maxdb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `viajes`
--

DROP TABLE IF EXISTS `viajes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `viajes` (
  `mid` int(11) NOT NULL,
  `pid` varchar(30) NOT NULL,
  `dia` date NOT NULL,
  `tipo` enum('partida','llegada') NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mid`,`pid`,`dia`),
  KEY `pid` (`pid`),
  CONSTRAINT `viajes_ibfk_1` FOREIGN KEY (`mid`) REFERENCES `marineros` (`mid`),
  CONSTRAINT `viajes_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `puertos` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viajes`
--

LOCK TABLES `viajes` WRITE;
/*!40000 ALTER TABLE `viajes` DISABLE KEYS */;
INSERT INTO `viajes` VALUES (22,'Puerto de Palos','1492-08-03','partida','Buscar ruta a Indias'),(29,'Puerto de Venecia','1260-01-01','partida','Buscar ruta comercial'),(29,'Puerto de Venecia','1270-01-01','llegada','Retorno de China y Oriente'),(31,'Puerto de San Lucar','1522-09-06','llegada','Primera circunnavegación del globo completada, retorno capitaneado por El Cano, Magallanes murio :('),(31,'Puerto de Sevilla','1519-08-10','partida','Busqueda de especias'),(32,'Puerto de Islandia','1000-01-01','partida','Busqueda de tierras'),(58,'Puerto de inglaterra','1768-01-01','partida','Viaje de observaciones atronomicas'),(58,'Puerto de Papeete','1769-04-13','llegada','Se documentó el tránsito de Venus sobre el Sol'),(71,'Puerto de inglaterra','1979-01-18','partida','Paseo'),(71,'Puerto de Islandia','1969-01-18','partida','Paseo'),(71,'Puerto de Palos','1929-01-18','partida','Paseo'),(71,'Puerto de Papeete','1989-01-18','llegada','Paseo'),(71,'Puerto de San Lucar','1959-01-18','llegada','Paseo'),(71,'Puerto de Sevilla','1949-01-18','partida','Paseo'),(71,'Puerto de Venecia','1939-01-18','partida','Paseo'),(71,'Puerto del Callao','2000-01-18','llegada','Paseo'),(71,'Puerto Principe','1999-01-18','llegada','Paseo'),(85,'Puerto del Callao','1947-04-28','partida','Expedicion antropologica'),(98,'Puerto Principe','1668-03-29','llegada','Saqueo de la ciudad');
/*!40000 ALTER TABLE `viajes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-17 11:59:00
