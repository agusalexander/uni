drop database tvclub;
create database tvclub;
use tvclub;
create table Premio(
idPremio integer, 
nombreDelPremio varchar(15),
 materialPremio varchar(15),
 primary key (idPremio)
 );
 create table Club(
idClub integer,
 nombreClub varchar(30),
 primary key (idclub)
 );
 create table Jugador(
idJugador integer,
 nombre varchar(15),
 apellido varchar(15),
 puesto varchar(15),
 primary key(idJugador));
 
create table ClubVendeJugador(
 idClub integer,
 idJugador integer,
 precioVenta integer,
 fechadeVenta date,
 primary key (idJugador,idClub,fechadeVenta),
 foreign key(idClub) references Club(idClub),
 foreign key(idJugador) references Jugador(idJugador)
 );

create table GanoPremio(
idJugador integer,
idPremio integer,
 fecha date,
 primary key(idJugador,iDpremio,fecha),
 foreign key(idJugador) references Jugador(idJugador),
 foreign key(iDpremio) references Premio(idPremio)
 );

