select tabla.nroPlanInformatica, tabla.cantMateriasPlanInformatica from(
    (select Carreras_Planes.idPlan as nroPlanInformatica, count(plandeestudio.idNroMateria) as cantMateriasPlanInformatica from  
		(select C.idCarrera, P.idPlan from 
		carrera C
        inner join
        plan P
        on (C.idCarrera = P.nroCarrera)
        where (C.nombreCarrera = 'informatica')
        )Carreras_Planes
        inner join
        plandeestudio
        on(Carreras_Planes.idPlan = plandeestudio.nroPlan)
        group by(idPlan)
    )tabla
);