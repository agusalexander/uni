select  nombrePadre, nombreMadre, Count(DNI_hijo) as 'hijos' from  
      (  
		(
			( select DNI as DNI_P, nombrePer as nombrePadre from personas where DNI in(select DNI_Padre from hijos) ) padres
			inner join 
			( select DNI as DNI_M, nombrePer as nombreMadre from personas where DNI in(select DNI_Madre from hijos) )  madres
			on (DNI_P != DNI_M)
		)
	    inner join 
            hijos
	    on (DNI_P = DNI_Padre and DNI_M = DNI_Madre)
      )
      group by nombrePadre, nombreMadre
;