#from principal import *
from extras import *
from configuracion import *

import random, math, os

def InitLemario(modo):
    if modo==1:
        lema=open("normal.txt","r")
    if modo==2:
        lema=open("lemaGaucho.txt","r")
    if modo==3:
        lema=open("nombres.txt","r")

    lemario=lema.readlines()
    lema.close()
    return lemario

def EligePalabra(lemario,salieron,dificultad):

    i=random.randrange(len(lemario))

    while (lemario[i] in salieron) or len(lemario[i])>=len(dificultad):  #Evita que salga la misma palabra
        i=random.randrange(len(lemario))
    salieron.append(lemario[i])     #La agrega a lista Salieron
    print(dificultad)
    print(lemario[i])
    return lemario[i]


def PalabraALista(palabra):
    listaPalabra=[]
    for letra in palabra:
        if letra!="\n":
            listaPalabra.append(str.lower(letra))
    return listaPalabra




def MezclaLetras(listaPalabra):
    listaRandom=listaPalabra[:]        #Crea una copia que no afecte la lista original
    random.shuffle(listaRandom)        #Mezcla la copia
    while listaRandom==listaPalabra:   #Se asegura de que no toque aleatoriamente la misma
        random.shuffle(listaRandom)
    return listaRandom                 #Devuelve la lista mezclada






def InitMat(mat,palabraLista):
    centra=7
    centrado=["" for i in range(centra)]
    palabraMezclada=centrado+MezclaLetras(palabraLista)
    i=0
    while i<len(palabraMezclada):
        if i<len(palabraMezclada):
            mat[0][i]=palabraMezclada[i]
        i+=1
    return mat

def OrdenaLetra(mat,palabraLista):
    centra=7
    backupletra=""
    ubicacionanterior=""
    for i in range(len(mat[0])):
        if mat[0][i]!="" and mat[0][i]!=palabraLista[i-centra]:
            backupletra=mat[0][i]
            ubicacionanterior=buscaLetra((palabraLista[i-centra]),mat,i)
            mat[0][ubicacionanterior]=backupletra
            mat[0][i]=palabraLista[i-centra]
            return mat



def PalabraCorrecta(candidata,palabraLista):
    listacandidata=PalabraALista(candidata)
    centra=0
    for i in range(len(listacandidata)):
        if listacandidata[i]=="":
            centra+=1
        elif palabraLista[i-centra]!=listacandidata[i]:
            return False
    return True

def Puntos(candidata):
    candidata=str.upper(candidata)
    consonantes=["B","C","D","F","G","H","L","M","N","P","R","S","T","V"]
    vocales=["A","E","I","O","U"]
    consonantesdificiles=["j","k","q","w","x","y","z"]
    puntos=0
    for letra in candidata:
        if Esta(letra,vocales):
            puntos+=1
        if Esta(letra,consonantes):
            puntos+=2
        if Esta(letra,consonantesdificiles):
            puntos+=5
        if len(candidata)>5: #Suma puntos por largo de palabra
            puntos+=5
    return puntos
#Adicionales
def buscaLetra(letra,mat,desde):  #Sirve para que la funcion ordenaletra encuentre donde (en la matriz) se encuentra la letra a remplazar por la correcta para no perderla.
    for i in range(desde,len(mat[0])):
        if mat[0][i]==letra:
            return i
def Esta(elemento,lista):
    cont=0
    for elemento1 in lista:
        if elemento1==elemento:
            cont+=1
    return(cont)


def CambiaFondo(segundos,modo):
    turquesa =(93, 193, 185)
    rojo=(255,0,0)
    verdeclaro=(0,255,0)
    azul=(0,0,255)
    colores=[turquesa,rojo,verdeclaro,azul]
    if modo=="tranqui":
            color=random.choice(colores)
#Si pasaron los 30 segundos
    elif modo=="fiesta":
        r=random.randrange(255)
        g=random.randrange(255)
        b=random.randrange(255)
        color=(r,g,b)

    return color

