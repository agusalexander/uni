def divProp(n):
    suma=0
    for i in range (1,n):
        if n%i==0:
            suma=suma+i
    return suma
def Perfecto(n):
    if divProp(n)==n:
        return True
    else:
        return False
def Abundante(n):
    if divProp(n)>n:
        return True
    else:
        return False
n=int(input("Ingrese un numero a analizar"))
print('La suma de todos los divisores propios de',n,'es igual a ',divProp(n))
if Perfecto(n):
    print('El numero es Perfecto')
elif Abundante(n):
    print('El numero es Abundante')
