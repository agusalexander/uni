def EsPrimo(n):
    x=0
    i=1
    while i<=n/2 and x<=1:
        if n%i==0:
            x+=1
        i+=1
    if x==1:
        return (True)
    else:
        return (False)

n=int(input('Ingrese un numero para encontrar el primo mas cercano'))
x=0
i=n
while x==0:
    i+=1

    if EsPrimo(i):
        x+=1
print(i,"Es el primo mas cercano de",n)