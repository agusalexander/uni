#Ejercicio 3: (3 puntos)
#Hacer un programa que indique si un número es abundante.
#Un número es abundante si la suma de los divisores propios supera al mismo número.
#Por ejemplo, 12 es abundante pues los divisores propios son 1 2 3 4 y 6 que suman en total 16 que es mayor a 12.

n=int(input('Ingrese un numero'))
i=1
abundante=0
print("Divisores=",end="")
while i<=n:
    if n%i==0: #Divisor
        abundante=abundante+i
        print( i,end=" ")
    i+=1
if abundante>2*n:
    print('\nEl numero',n,'es abundante, la suma de sus divisores es:',abundante)
else:
    print('El numero ',n,' NO es abundante, la suma de sus divisores es:',abundante)



