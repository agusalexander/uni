def cobertura(cliente):
    if cliente=="Cachito":
        return "Plata"
    elif cliente=="Juan":
        return "Oro"
    elif cliente=="Tito":
        return "Plata"
    else:
        return False
def usados(cliente):
    if cliente=="Cachito":
        return 3
    elif cliente=="Juan":
        return 7
    elif cliente=="Tito":
        return 5

def radioDeCobertura(localidad):
    if localidad=="Moreno":
        return False
    if localidad=="San Miguel":
        return True

def Pagara(cliente,localidad):
    if not cobertura(cliente):
        return "La persona no es un cliente en servicio"
    if usados(cliente)>=5:
        if cobertura(cliente)=="Plata":
            if not radioDeCobertura(localidad):
                return "$80"
            elif radioDeCobertura(localidad):
                return "$50"
        elif cobertura(cliente)=="Oro":
            if not radioDeCobertura(localidad):
                return "$30"
            elif radioDeCobertura(localidad):
                return "Gratis"
    elif not radioDeCobertura(localidad):
        return "$30"
    elif usados(cliente)<5 and radioDeCobertura(localidad):
        return "Gratis"
print(Pagara("Tito","San Miguel"))