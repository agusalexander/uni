def capicuacirular(palabra,n):
    listaPalabra=[]
    reloj=[]
    contrareloj=[]
    x=0
    for letra in palabra:
        listaPalabra.append(letra)

    for i in range(n,len(listaPalabra)+n):
        if i>=len(listaPalabra):
            i=i-len(listaPalabra)
        reloj.append(listaPalabra[i])

    for i in range(n-1,-1*(len(listaPalabra)+1),-1):
        if i<0:
            i=i+len(listaPalabra)
        contrareloj.append(listaPalabra[i])

    for i in range(len(listaPalabra)):
        if reloj[i]!=contrareloj[i]:

            return False
    return True

palabra=input('Ingrese Palabra')
x=0
for i in range(1,len(palabra)):
    if capicuacirular(palabra,i):
        print('La palabra es capicua circular partiendo desde la posicion', i)
        x=1

if x==0:
    print("La palabra no es capicua circular en ninguna posicion")
