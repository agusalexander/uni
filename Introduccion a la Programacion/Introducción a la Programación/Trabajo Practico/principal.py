#! /usr/bin/env python
import os, random, sys, math

import pygame
from pygame.locals import *

from configuracion import *
from funciones import *
from extras import *
#Funcion principal
def main():



    #Centrar la ventana y despues inicializar pygame
    os.environ["SDL_VIDEO_CENTERED"] = "1"
    pygame.init()
    Ancho=RESOLUCION[0]
    Alto=RESOLUCION[1]
    #Preparar la ventana
    pygame.display.set_caption("MEZCLA PALABRAS")
    screen = pygame.display.set_mode((Ancho,Alto))#((ANCHO, ALTO))

    #DIFICULTAD
    dificultad="Normal"
    tiempodeOrdenaletra=len(dificultad)

    #tiempo total del juego
    gameClock = pygame.time.Clock()
    totaltime = TIEMPO_MAX
    segundos = TIEMPO_MAX
    T=0

    #Cosas
    fps = FPS_inicial
    lemario = InitLemario(2)
    salieron = []
    palabraLista = PalabraALista(EligePalabra(lemario,salieron,dificultad))
    mat=[ ["" for j in range(COLUMNAS)] for i in range(len(palabraLista))]
    mat=InitMat(mat,palabraLista)
    ganador = ""
    #Puntos
    puntos = 0
    intentos = INTENTOS_INICIALES


    #SONIDOS
    Error=pygame.mixer.Sound("Error.wav")
    Bien=pygame.mixer.Sound("Noice.wav")
    Cambio=pygame.mixer.Sound("Cambio.wav")
    pygame.mixer.music.load("Music.wav")
    pygame.mixer.music.play(-1)

    #solucionar problema de espera, sin usar wait
    segundos2=0
    segundos3=0
    #inputField
    #  0: palabra
    #  1: fila
    #  2: columna
    #  3: direccion
    inputField = 0
    palabra = ""
    fil = col = ""
    direccion = [0,0]
    showDireccion = ""
    dibujarMatriz(screen, mat, "0", intentos, palabra, fil, col, "", ganador, segundos, inputField)
    #BOOLEANOS
    Inicio=True
    GameOver=False
    Salir=False
    unlockExit=False






    while not Salir:
        while GameOver and not Salir: #Menu Inicio y Game over
            screen.fill(colorfondo)
            pygame.mixer.music.fadeout(3000) # va bajando el volumen
            #Musica de menu?
            mensajecentrado("Jugar de Nuevo",(0,0,0),[0,0])
            mensajecentrado("Salir",(0,0,0),[0,50])
            for e in pygame.event.get():
                if e.type == pygame.KEYDOWN:
                    if e.key==pygame.K_RETURN and unlockExit==True:
                        Salir=True
                    elif e.key == pygame.K_RETURN:
                        GameOver=False
                        gameClock = pygame.time.Clock()
                        totaltime = TIEMPO_MAX
                        segundos = TIEMPO_MAX
                        T=0
                        puntos = 0
                        intentos = INTENTOS_INICIALES
                        inputField = 0
                        palabraLista = PalabraALista(EligePalabra(lemario,salieron,dificultad))
                        mat = [ ["" for j in range(COLUMNAS)] for i in range(len(palabraLista))]
                        InitMat(mat,palabraLista)

                    elif e.key == pygame.K_DOWN:
                        unlockExit=True #mover el cuadradito del menu
                if e.type == QUIT:
                   Salir=True
            pygame.display.update()


        #ReInicializa

       #Ciclo principal
        while not GameOver and not Salir:


            if True:
            	fps = 3

            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():

            	#QUIT es apretar la X en la ventana
            	if e.type == QUIT:
                        Salir=True
            	#Ver si fue apretada alguna tecla
            	if e.type == KEYDOWN:
            		#Ingresar palabra
            		if inputField < 1:
            			letra = LetraApretada(e.key)
            			palabra += letra
            			if e.key == K_BACKSPACE:
            				palabra = palabra[0:len(palabra)-1]
            			if e.key == K_RETURN and palabra != "":
            				inputField = 1

            if inputField == 1:         ##    #Esta comentado para testear el resto
                                    ##    # cuando ande el resto, descomentar y sacar la siguiente linea
                                    ##    #if EnDiccionario(palabra, lemario) and PalabraEnMatriz(palabra, mat, int(fil), int(col), direccion[0], direccion[1]):
                                    ##    # PalabraEnMatriz(palabra, mat) and EnDiccionario(palabra,lemario) and NoRepetida(palabra,salieron):


                if PalabraCorrecta(palabra,palabraLista):
                    puntos += Puntos(palabra)

                    pygame.mixer.Sound.play(Bien)
                    inputField = 0
                    palabraLista = PalabraALista(EligePalabra(lemario,salieron,dificultad))
                    mat = [ ["" for j in range(COLUMNAS)] for i in range(len(palabraLista))]
                    InitMat(mat,palabraLista)
                    #########
                else:
                    pygame.mixer.Sound.play(Error)
                    inputField = -1
                    intentos -= 1
                palabra = ""
                fil = col = ""
                direccion = [0,0]
                showDireccion = ""

            gameClock.tick(fps)
            T += gameClock.get_time()

            segundos = TIEMPO_MAX - pygame.time.get_ticks()/1000

            if segundos>(totaltime/2)+1:
                if Inicio==True:
                    colorfondo=CambiaFondo(segundos,"tranqui")   #Cambia colores predeterminados
                    Inicio=False
                    segundos2=segundos
                elif not Inicio and int(segundos)%5==0 and abs(segundos2-segundos)>1:
                    segundos2=segundos
                    colorfondo=CambiaFondo(segundos,"tranqui")

            else:
                colorfondo=CambiaFondo(segundos,"fiesta")   #Fiestaaa de color random

            screen.fill(colorfondo)
                            #Con esto se puede setear la difcicultad reduciendo tiempodeOrdenaletra                 #Se fija que no entre mas de una vez en 1 segundo
            if int(segundos)%tiempodeOrdenaletra == 0 and int(segundos) != 0 and int(segundos) != totaltime and (abs(segundos-segundos3)>=tiempodeOrdenaletra-1): #Renueva la matriz cada 10 segundos
                segundos3=segundos
                print(palabraLista)
                if not PalabraCorrecta(mat[0],palabraLista):
                    OrdenaLetra(mat,palabraLista)   #Ordena letra
                    pygame.mixer.Sound.play(Bien) # SONIDO CUANDO UNA LETRA SE ORDENA


                if PalabraCorrecta(mat[0],palabraLista):    #Si luego de ordenar La palabra queda resuelta resta 5 puntos y lo escribe en pantalla
                    mensajecentrado("No Adivinaste!",(0,0,0),[0,-150])
                    puntos=puntos-5
                    pygame.mixer.Sound.play(Error)
                    pygame.display.flip()

            #Condiciones para que sea Game Over
            if segundos <= fps/1000 or intentos<=0:

                GameOver=True

            #Dibujar de nuevo todo
            dibujarMatriz(screen, mat, str(puntos), intentos, palabra, fil, col, ShowDireccion(direccion), ganador, segundos, inputField)


            pygame.display.flip()



    pygame.quit() #Sale del programa
    return

#Programa Pirncipal ejecuta Main
if __name__ == "__main__":
	main()