# Usuario ingresa un numero
# Indicar cuantos terminos debe tener la sucesion para ser mayor al numero ingresado por el usuario
# Imprimir la susecion
# Sucesion = ( 1 ) + ( 4 ** 2 / 3 ) + ( 6 ** 3 / 4 ) + ( 8 ** 4 / 5 ) + ...

numero = int(input("ingrese un numero: "))
a = 2
b = 1
c = 2
cantidad = 1
sucesion = 1

while numero >= sucesion:
    a = a + 2
    b = b + 1
    c = c + 1
    num = a**b
    den = c
    termino = num/den
    cantidad = cantidad + 1
    sucesion = sucesion + termino


print("El valor final de la sucesion es:",sucesion)
print("La sucesion necesito de",cantidad,"terminos para superar al numero ingresado","(",numero,")")