num1 = int(input("Ingrese el primer numero\n"))
num2 = int(input("Ingrese el segundo numero\n"))
# Seleccionamos el mayor y el menor y los asignamos
# a las variables "a" y "b" respectivamente
a = max(num1, num2)
b = min(num1, num2)
# Realizamos el ciclo encargado de hacer las iteraciones
while b!=0:
    res = b
    b = a%b
    a = res
#el divisor en la segunda iteracion es el resto de la primer iteracion
# Mostramos el resultado en pantalla
print("El M.C.D. entre",num1,"y",num2,"es",res)
