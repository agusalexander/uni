
public class Domicilio {
    private String calle;
    private int numero;
    private String depto;
    private String codPostal;
    private String localidad;
    private Provincia provincia;
    public Domicilio (String Calle, int Numero, String Dpto, String CodigoPostal, String Localidad, Provincia Provincia){
        this.calle=Calle;
        this.numero=Numero;
        this.depto=Dpto;
        this.codPostal=CodigoPostal;
        this.localidad=Localidad;
        this.provincia=Provincia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDepto() {
        return depto;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
}
