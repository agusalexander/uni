package dao.intento4.dao;

import org.neodatis.odb.Objects;
import org.neodatis.odb.core.query.IQuery;

public interface DAO<T> {
	
	public void save(T t);
	public void eraseAll(T t);
	public Objects<T> getUsingQuery(IQuery query);

}
