package DataObjects;

/**
 * Created by Max on 3/24/2017.
 */
public class Person  {

    private String name, surname;
    private String dayOfBirth;
    private Adress adress;
    private int edad;

    public Person(String name, String surname, String dayOfBirth, Adress adress,int edad){
        this.name = name;
        this.surname = surname;
        this.dayOfBirth = dayOfBirth;
        this.adress = adress;
        this.edad = edad;
    }

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getSurname() {return surname;}

    public void setSurname(String surname) {this.surname = surname;}

    public Adress getAdress() {return adress;}

    public void setAdress(Adress adress) {this.adress = adress;}

    public String getDayOfBirth() {return dayOfBirth;}

    public void setDayOfBirth(String dayOfBirth) {this.dayOfBirth = dayOfBirth;}

    public int getEdad(){return this.edad;}

    public void setEdad(int edad){this.edad = edad;}

    @Override
    public String toString() {
        return " nombre "+ this.name + " apellido "+ this.surname+ " edad " +this.edad + " años ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (surname != null ? !surname.equals(person.surname) : person.surname != null) return false;
        if (dayOfBirth != null ? !dayOfBirth.equals(person.dayOfBirth) : person.dayOfBirth != null) return false;
        return adress != null ? adress.equals(person.adress) : person.adress == null;

    }


}
