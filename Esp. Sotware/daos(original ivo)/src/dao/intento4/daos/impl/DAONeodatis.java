package dao.intento4.daos.impl;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;

import dao.intento4.dao.DAO;

public class DAONeodatis<T> implements DAO<T>{

	
	public void guardar(T t){
		ODB odb  = ODBFactory.open("mibase");
		odb.store(t);
		odb.close();
	}
	
	public void eliminar(T t){
		ODB odb  = ODBFactory.open("mibase");
		odb.store(t);
		odb.close();
	}

	
}
