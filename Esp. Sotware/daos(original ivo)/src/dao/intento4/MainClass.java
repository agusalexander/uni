package dao.intento4;

import dao.dm.Cliente;
import dao.intento4.dao.ClienteDAO;
import dao.intento4.daos.impl.ClienteDAONeodatis;

public class MainClass {

	public static void main(String[] args) {
		Cliente c = new Cliente(302505515, "Ferreteria Tito");
		
		ClienteDAO dao = new ClienteDAONeodatis();
		dao.guardar(c);

	}

}
