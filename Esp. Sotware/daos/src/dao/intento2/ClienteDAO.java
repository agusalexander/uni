package dao.intento2;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;

import dao.dm.Cliente;
import dao.dm.Direccion;

public class ClienteDAO {
	
	private ODB odb; 
	
	public ClienteDAO(){
		
	}
	
	public void guardar(Cliente c){
		odb  = ODBFactory.open("mibase");
		odb.store(c);
		odb.close();
	}

}
