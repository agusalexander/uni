package dao.intento1;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;

import dao.dm.Cliente;
import dao.dm.Direccion;

public class DAO {
	
	private ODB odb; 
	
	public DAO(){
		
	}
	
	public void guardar(Cliente c){
		odb  = ODBFactory.open("mibase");
		odb.store(c);
		odb.close();
	}
	
	public void guardar(Direccion d){
		odb  = ODBFactory.open("mibase");
		odb.store(d);
		odb.close();
	}

   
}
