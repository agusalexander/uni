import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;

/**
 * Created by Max on 3/24/2017.
 */
public class GettingObjects {
    public static void main(String[] args) {
        ODB odb = ODBFactory.open("clase1db");
// Recuperamos todos los clientes
        Objects<Cliente> clientes = odb.getObjects(Cliente.class);
// y los mostramos
        for (Cliente act : clientes) {
            act = clientes.next();
            System.out.println(act);
        }
// cerramos la bd
        odb.close();
    }
}
