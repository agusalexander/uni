package Ejercicio2;

/**
 * Created by Max on 3/24/2017.
 */
public class Province implements Keepable{

    private String name;

    public Province(String name){
        this.name = name;
    }

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Province province = (Province) o;

        return name != null ? name.equals(province.name) : province.name == null;

    }


}
