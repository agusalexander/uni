package Ejercicio2;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;

/**
 * Created by Max on 3/24/2017.
 */
public abstract class DAO {
    public static void guardar(Keepable object){
        ODB dataBase = null;

        try{
            dataBase = ODBFactory.open("Domicilio de Personas");
            dataBase.store(object);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(dataBase != null)
                dataBase.close();
        }
    }

    public static void borrar(Keepable object){
        ODB dataBase = null;
        Keepable aux = null;
        try{
            dataBase = ODBFactory.open("Domicilio de Personas");
            Objects<Keepable> objects = dataBase.getObjects(object.getClass());
            for(Keepable data : objects){
                if(data.equals(object)){
                    aux = data;
                }
            }
            if(aux != null)
                dataBase.deleteCascade(aux);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(dataBase != null)
                dataBase.close();
        }
    }
}
