package Ejercicio2;

/**
 * Created by Max on 3/24/2017.
 */
public class Adress implements Keepable {

    private String street, locality;
    private int streetNumber, department, postCode;
    private Province province;


    public static class AdressBuilder{
        private String street, locality;
        private int streetNumber, department, postCode;
        private Province province;

        public Adress build(){
            return new Adress(this);
        }

        public AdressBuilder street(String street) {
            this.street = street;
            return this;
        }

        public AdressBuilder locality(String locality) {
            this.locality = locality;
            return this;
        }

        public AdressBuilder streetNumber(int streetNumber) {
            this.streetNumber = streetNumber;
            return this;
        }

        public AdressBuilder department(int department) {
            this.department = department;
            return this;
        }

        public AdressBuilder postCode(int postCode) {
            this.postCode = postCode;
            return this;
        }

        public AdressBuilder province(Province province) {
            this.province = province;
            return this;
        }
    }

    private Adress(AdressBuilder builder){
        this.street = builder.street;
        this.locality = builder.locality;
        this.streetNumber = builder.streetNumber;
        this.department = builder.department;
        this.postCode = builder.postCode;
        this.province = builder.province;
    }

    public String getStreet() {return street;}

    public void setStreet(String street) {this.street = street;}

    public String getLocality() {return locality;}

    public void setLocality(String locality) {this.locality = locality;}

    public int getStreetNumber() {return streetNumber;}

    public void setStreetNumber(int streetNumber) {this.streetNumber = streetNumber;}

    public int getDepartment() {return department;}

    public void setDepartment(int department) {this.department = department;}

    public int getPostCode() {return postCode;}

    public void setPostCode(int postCode) {this.postCode = postCode;}

    public Province getProvince() {return province;}

    public void setProvince(Province province) {this.province = province;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Adress adress = (Adress) o;

        if (streetNumber != adress.streetNumber) return false;
        if (department != adress.department) return false;
        if (postCode != adress.postCode) return false;
        if (street != null ? !street.equals(adress.street) : adress.street != null) return false;
        if (locality != null ? !locality.equals(adress.locality) : adress.locality != null) return false;
        return province != null ? province.equals(adress.province) : adress.province == null;

    }
}
