package Ejercicio2;

/**
 * Created by Max on 3/24/2017.
 */
public class MainClass {

    public static void main(String [] args){
        Province bsAs = new Province ("Washington");
        Adress adress1 = new Adress.AdressBuilder().province(bsAs).
                         locality("Nueva York").street("Broadway").
                         streetNumber(3000).postCode(2134).department(0).build();
        Person person = new Person("Juanito", "Arcoiris", "99/99/9999",adress1);

        DAO.guardar(person);
        DAO.borrar(person);



    }
}
