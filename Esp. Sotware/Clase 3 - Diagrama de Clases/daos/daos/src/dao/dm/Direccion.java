package dao.dm;

public class Direccion {
	
	
	private String domicilio;
	
	public Direccion(String domicilio) {
		
		this.domicilio = domicilio;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

}
