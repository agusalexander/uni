package dao.dm;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	
	
	private long cuit;
	private String razonSocial;
	private Direccion dir;
	private List<Direccion> dirDeEntrega = new ArrayList<Direccion>();

	
	public Cliente(long cuit, String razonSocial) {
		this.cuit = cuit;
		this.razonSocial = razonSocial;
	}
	
	public long getCuit() {
		return cuit;
	}
	public void setCuit(long cuit) {
		this.cuit = cuit;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public Direccion getDir() {
		return dir;
	}

	public void setDir(Direccion dir) {
		this.dir = dir;
	}

	public List<Direccion> getDirDeEntrega() {
		return dirDeEntrega;
	}

	public void setDirDeEntrega(List<Direccion> dirDeEntrega) {
		this.dirDeEntrega = dirDeEntrega;
	}

}
