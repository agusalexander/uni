package dao.intento4.daos.impl;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;
import org.neodatis.odb.Objects;
import org.neodatis.odb.core.query.IQuery;
import org.neodatis.odb.core.query.criteria.Where;
import org.neodatis.odb.impl.core.query.criteria.CriteriaQuery;

import dao.dm.Cliente;
import dao.intento4.dao.DAO;

public class DAONeodatis<T> implements DAO<T>{

	
	public void guardar(T t){
		ODB odb  = ODBFactory.open("mibase");
		odb.store(t);
		odb.close();
	}
	

	public void eliminar(T t){
		ODB odb  = ODBFactory.open("mibase");
		odb.store(t);
		odb.close();
	}

	
}
