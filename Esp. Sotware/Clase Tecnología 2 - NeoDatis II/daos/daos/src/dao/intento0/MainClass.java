package dao.intento0;

import org.neodatis.odb.ODB;
import org.neodatis.odb.ODBFactory;

import dao.dm.Cliente;

public class MainClass {

	public static void main(String[] args) {
		ODB odb = null; 
		try {
			odb  = ODBFactory.open("mibase");
			Cliente c = new Cliente(302505515, "Ferreteria Tito");
			odb.store(c);
			
		} finally {
			if (odb != null)
				odb.close();
		}
	
	}

}
